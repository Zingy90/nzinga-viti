# Simple Web Application CI/CD Pipeline with GitLab and AWS

This project demonstrates how to set up a CI/CD pipeline using GitLab CI/CD to deploy a simple web application to AWS. The pipeline will build, test, and deploy the application to an S3 bucket and an EC2 instance using AWS CodeBuild and CodeDeploy.

## Table of Contents
1. [Project Overview](#project-overview)
2. [Prerequisites](#prerequisites)
3. [Architecture](#architecture)
4. [Setup Instructions](#setup-instructions)
5. [Pipeline Configuration](#pipeline-configuration)
6. [Usage](#usage)
7. [Contributing](#contributing)


## Project Overview

This project sets up a CI/CD pipeline using GitLab CI/CD to automate the deployment of a simple web application. The pipeline includes the following stages:
- **Build**: Compiles the application code.
- **Test**: Runs unit tests on the application.
- **Deploy**: Deploys the application to an S3 bucket and an EC2 instance.

## Prerequisites

- AWS Account
- GitLab Account
- AWS CLI installed and configured
- Basic knowledge of AWS services (S3, EC2, CodeBuild, CodeDeploy)
- IAM roles and policies set up for CodeBuild and CodeDeploy

## Architecture

The architecture of the CI/CD pipeline is as follows:
1. **Source Code**: Hosted in a GitLab repository.
2. **Build and Test**: CodeBuild compiles the code and runs tests.
3. **Deploy**: CodeDeploy deploys the application to an S3 bucket and an EC2 instance.

## Setup Instructions

### Step 1: Create an S3 Bucket

1. Open the AWS Management Console.
2. Navigate to the S3 service.
3. Create a new bucket (e.g., `my-web-app-bucket`).

### Step 2: Create an EC2 Instance

1. Open the AWS Management Console.
2. Navigate to the EC2 service.
3. Launch a new EC2 instance with the Amazon Linux 2 AMI.
4. Attach an IAM role with the `AmazonS3ReadOnlyAccess` and `AmazonEC2RoleforAWSCodeDeploy` policies.

### Step 3: Set Up CodeDeploy

1. Open the AWS Management Console.
2. Navigate to the CodeDeploy service.
3. Create a new application and deployment group.

### Step 4: Configure GitLab CI/CD

1. In your GitLab repository, navigate to **Settings > CI/CD > Variables**.
2. Add the following variables:
   - `AWS_ACCESS_KEY_ID`: Your AWS access key ID.
   - `AWS_SECRET_ACCESS_KEY`: Your AWS secret access key.
   - `S3_BUCKET`: The name of your S3 bucket.
   - `DEPLOYMENT_GROUP`: The name of your CodeDeploy deployment group.
   - `APPLICATION_NAME`: The name of your CodeDeploy application.

## Pipeline Configuration

Create a `.gitlab-ci.yml` file in the root of your repository with the following content:

```yaml
stages:
  - build
  - test
  - deploy

variables:
  AWS_DEFAULT_REGION: us-east-1

before_script:
  - apt-get update -y
  - apt-get install -y python3-pip
  - pip3 install awscli

build:
  stage: build
  script:
    - echo "Building the application..."
    - # Add your build commands here

test:
  stage: test
  script:
    - echo "Running tests..."
    - # Add your test commands here

deploy:
  stage: deploy
  script:
    - echo "Deploying to S3..."
    - aws s3 sync . s3://$S3_BUCKET --exclude ".git/*" --delete
    - echo "Deploying to EC2..."
    - aws deploy create-deployment --application-name $APPLICATION_NAME --deployment-group-name $DEPLOYMENT_GROUP --s3-location bucket=$S3_BUCKET,key=app.zip,bundleType=zip

Usage
Commit and Push: Commit and push your changes to the GitLab repository. The pipeline will automatically trigger and execute the build, test, and deploy stages.
Monitor Pipeline: Monitor the pipeline execution in the GitLab CI/CD interface.
Verify Deployment: Verify that the application has been deployed to the S3 bucket and the EC2 instance.
Contributing
Fork the repository.
Create a new branch:
bash
git checkout -b feature-branch

Make your changes and commit them:
bash
git commit -m "Add new feature"

Push to the branch:
bash
git push origin feature-branch

Open a pull request.