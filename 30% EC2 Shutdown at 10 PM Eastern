# 30% EC2 Shutdown at 10 PM Eastern

This project automates the shutdown of 30% of EC2 instances at 10 PM Eastern Time using AWS Lambda and Amazon EventBridge Scheduler.

## Table of Contents
1. [Project Overview](#project-overview)
2. [Prerequisites](#prerequisites)
3. [Installation](#installation)
4. [Usage](#usage)


## Project Overview

This project demonstrates how to use AWS Lambda and Amazon EventBridge Scheduler to stop 30% of your EC2 instances at a specific time every day. The Lambda function is triggered by an EventBridge rule set to run at 10 PM Eastern Time.

## Prerequisites

- AWS Account
- AWS CLI installed and configured
- Basic knowledge of AWS services (EC2, Lambda, IAM, EventBridge)


## Installation

### Step 1: Create IAM Policy and Role

1. **Create IAM Policy**:
   ```json
   {
     "Version": "2012-10-17",
     "Statement": [
       {
         "Effect": "Allow",
         "Action": [
           "logs:CreateLogGroup",
           "logs:CreateLogStream",
           "logs:PutLogEvents"
         ],
         "Resource": "arn:aws:logs:*:*:*"
       },
       {
         "Effect": "Allow",
         "Action": [
           "ec2:StopInstances",
           "ec2:DescribeInstances"
         ],
         "Resource": "*"
       }
     ]
   } 
Create IAM Role:
Go to the IAM console, create a new role, and attach the above policy to it.
Step 2: Create Lambda Function
Create Lambda Function:
Open the Lambda console and create a new function.
Choose "Author from scratch" and use Python 3.9 as the runtime.
Attach the IAM role created in Step 1.
Lambda Function Code:
python
import boto3
import random

def stop_instances_by_percentage(percentage, region):
    ec2 = boto3.client('ec2', region_name=region)
    instances = ec2.describe_instances(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
    instance_ids = [instance['InstanceId'] for reservation in instances['Reservations'] for instance in reservation['Instances']]
    
    # Calculate the number of instances to stop
    num_to_stop = int(len(instance_ids) * (percentage / 100))
    instances_to_stop = random.sample(instance_ids, num_to_stop)
    
    if instances_to_stop:
        ec2.stop_instances(InstanceIds=instances_to_stop)
        print(f'Stopped instances: {instances_to_stop}')
    else:
        print('No instances to stop.')

def lambda_handler(event, context):
    region = 'us-east-1'  # Change to your region
    stop_instances_by_percentage(30, region)

Deploy the Lambda Function:
Paste the code into the Lambda function editor and deploy it.
Step 3: Set Up EventBridge Rule
Create EventBridge Rule:
Open the EventBridge console and create a new rule.
Set the rule type to "Schedule" and configure it to run at 10 PM Eastern Time every day.
Configure the Schedule:
Use a cron expression to set the schedule: cron(0 2 * * ? *) (10 PM Eastern Time is 2 AM UTC).
Usage
Test the Lambda Function:
Manually test the Lambda function to ensure it stops the correct percentage of instances.
Monitor Logs:
Use Amazon CloudWatch to monitor the logs and verify that the function is running as expected.