# Procore Plus Real-Time Auditing Solution

## Overview
This project provides a proof of concept (POC) for a real-time auditing solution that uses AWS CloudTrail logs to trigger a CloudWatch alarm whenever an IAM change is made in the company's AWS account. The solution ensures that any changes to IAM policies, roles, or users are promptly detected and alerts are sent out.

## Prerequisites
- AWS account with appropriate permissions
- AWS CLI configured
- AWS CloudFormation
- Amazon SNS topic for notifications

## Architecture
1. **CloudTrail**: Captures all API calls related to IAM changes.
2. **CloudWatch Logs**: Receives log data from CloudTrail.
3. **Metric Filter**: Filters specific IAM change events from CloudWatch Logs.
4. **CloudWatch Alarm**: Triggers based on the metric filter.
5. **Amazon SNS**: Sends notifications when the alarm is triggered.

## Steps to Implement

# Step 1: Create an SNS Topic
aws sns create-topic --name IAM_Changes_Alarm_Topic

# Capture the ARN of the SNS Topic for later use
SNS_TOPIC_ARN=$(aws sns create-topic --name IAM_Changes_Alarm_Topic --query 'TopicArn' --output text)

# Step 2: Create a CloudTrail Trail
aws cloudtrail create-trail --name ProcorePlusTrail --s3-bucket-name <YourS3BucketName>
aws cloudtrail start-logging --name ProcorePlusTrail

# Step 3: Create a CloudWatch Log Group
aws logs create-log-group --log-group-name ProcorePlusLogGroup

# Step 4: Configure CloudTrail to Send Logs to CloudWatch
aws cloudtrail update-trail --name ProcorePlusTrail \
  --cloud-watch-logs-log-group-arn arn:aws:logs:<region>:<account-id>:log-group:ProcorePlusLogGroup \
  --cloud-watch-logs-role-arn arn:aws:iam::<account-id>:role/<role-name>

# Step 5: Create a Metric Filter
aws logs put-metric-filter --log-group-name ProcorePlusLogGroup \
  --filter-name IAMChangeFilter \
  --filter-pattern '{ ($.eventName = DeleteUserPolicy) || ($.eventName = PutUserPolicy) || ($.eventName = AttachUserPolicy) || ($.eventName = DetachUserPolicy) || ($.eventName = DeleteRolePolicy) || ($.eventName = PutRolePolicy) || ($.eventName = AttachRolePolicy) || ($.eventName = DetachRolePolicy) }' \
  --metric-transformations metricName=IAMChangeCount,metricNamespace=ProcorePlusMetrics,metricValue=1

# Step 6: Create a CloudWatch Alarm
aws cloudwatch put-metric-alarm --alarm-name IAMChangeAlarm \
  --metric-name IAMChangeCount \
  --namespace ProcorePlusMetrics \
  --statistic Sum \
  --period 300 \
  --threshold 1 \
  --comparison-operator GreaterThanOrEqualToThreshold \
  --evaluation-periods 1 \
  --alarm-actions $SNS_TOPIC_ARN

# Step 7: Subscribe to the SNS Topic
aws sns subscribe --topic-arn $SNS_TOPIC_ARN --protocol email --notification-endpoint <your-email@example.com>
